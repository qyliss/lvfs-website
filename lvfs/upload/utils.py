#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

import hashlib
import tempfile
import subprocess
import glob
import os
from typing import List, Optional
from flask import render_template

from sqlalchemy import or_
from sqlalchemy.exc import IntegrityError

from cabarchive import CorruptionError, CabArchive, CabFile

from lvfs import db, ploader, tq

from lvfs.agreements.models import Agreement
from lvfs.categories.models import Category
from lvfs.licenses.models import License
from lvfs.emails import send_email
from lvfs.firmware.models import (
    Firmware,
    FirmwareEvent,
    FirmwareEventKind,
    FirmwareRevision,
)
from lvfs.firmware.utils import _firmware_delete, _sign_fw
from lvfs.metadata.models import Remote
from lvfs.metadata.utils import _regenerate_and_sign_metadata_remote
from lvfs.protocols.models import Protocol
from lvfs.tests.utils import _async_test_run_for_firmware
from lvfs.upload.uploadedfile import (
    FileNotSupported,
    FileTooLarge,
    FileTooSmall,
    MetadataInvalid,
    UploadedFile,
)
from lvfs.users.models import User
from lvfs.util import (
    _fix_component_name,
    _get_settings,
)
from lvfs.verfmts.models import Verfmt
from .models import Upload


def _user_can_upload(user: User) -> bool:

    # never signed anything
    if not user.agreement:
        return False

    # is it up to date?
    agreement = db.session.query(Agreement).order_by(Agreement.version.desc()).first()
    if not agreement:
        return False
    if user.agreement.version < agreement.version:
        return False

    # works for us
    return True


def _filter_fw_by_id_guid_version(
    fws: List[Firmware], appstream_id: str, provides_value: str, release_version: str
) -> Optional[Firmware]:
    for fw in fws:
        if fw.is_deleted:
            continue
        for md in fw.mds:
            if md.appstream_id != appstream_id:
                continue
            for guid in md.guids:
                if guid.value == provides_value and md.version == release_version:
                    return fw
    return None


def _repackage_archive(buf: bytes) -> bytes:
    """Unpacks an archive (typically a .zip) into a CabArchive object"""

    # decompress to a temp directory
    with tempfile.TemporaryDirectory(prefix="foreignarchive_") as dest:
        with tempfile.NamedTemporaryFile(
            mode="wb", prefix="foreignarchive_", suffix=".zip", dir=None, delete=True
        ) as src:
            src.write(buf)
            src.flush()

            # extract
            with subprocess.Popen(
                ["/usr/bin/bsdtar", "--directory", dest, "-xvf", src.name],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
            ) as ps:
                if ps.wait() != 0:
                    if ps.stderr:
                        raise IOError(
                            "Failed to extract: %s" % ps.stderr.read().decode()
                        )
                    raise IOError("Failed to extract")

        # add all the fake CFFILE objects
        cabarchive = CabArchive()
        for fn in glob.glob(dest + "/**/*.*", recursive=True):
            with open(fn, "rb") as f:
                fn = os.path.basename(fn.replace("\\", "/"))
                cabarchive[fn] = CabFile(f.read())

    # return blob
    return cabarchive.save()


def _upload_firmware_raw(up: Upload, sign: bool = True) -> Firmware:

    # verify the user can upload
    if not _user_can_upload(up.user):
        raise PermissionError("User has not signed legal agreement")

    # security check
    if not up.vendor.check_acl("@upload", user=up.user):
        raise PermissionError(
            "Failed to upload file for vendor: "
            "User with vendor {} cannot upload to vendor {}".format(
                up.user.vendor.group_id, up.vendor.group_id
            ),
        )

    # not correct parameters
    if up.target not in ["private", "embargo", "testing"]:
        raise KeyError("Target not valid")

    # find remote, creating if required
    remote_name = up.target
    if remote_name == "embargo":
        remote = up.vendor.remote
    else:
        remote = db.session.query(Remote).filter(Remote.name == remote_name).first()
    if not remote:
        raise FileNotSupported("No remote for target {}".format(remote_name))

    # add a new revision actually in .cab format
    if up.revision.filename.endswith(".zip"):
        revision = FirmwareRevision(
            filename=up.revision.filename.replace(".zip", ".cab")
        )
        revision.write(_repackage_archive(up.revision.blob))
    else:
        revision = up.revision

    # load in the archive
    try:
        ufile = UploadedFile(
            enable_inf_parsing=up.vendor.default_inf_parsing,
        )
        for cat in db.session.query(Category):
            ufile.category_map[cat.value] = cat.category_id
        for pro in db.session.query(Protocol):
            ufile.protocol_map[pro.value] = pro.protocol_id
        for verfmt in db.session.query(Verfmt):
            ufile.version_formats[verfmt.value] = verfmt
        for lic in db.session.query(License):
            ufile.license_map[lic.value] = lic
        ufile.parse(revision)
    except (
        CorruptionError,
        FileNotSupported,
        FileTooLarge,
        FileTooSmall,
        MetadataInvalid,
    ) as e:
        raise FileNotSupported(str(e)) from e

    # add the ODM as a string used for searching, but that does not get included in the XML
    for md in ufile.fw.mds:
        vendor = up.user.vendor
        md.add_keywords_from_string(vendor.group_id, priority=-1)
        if vendor.display_name:
            md.add_keywords_from_string(vendor.display_name, priority=-1)

    # check the file does not already exist
    fw_old = (
        db.session.query(Firmware)
        .filter(
            or_(
                Firmware.checksum_upload_sha1 == ufile.fw.checksum_upload_sha1,
                Firmware.checksum_upload_sha256 == ufile.fw.checksum_upload_sha256,
            )
        )
        .first()
    )
    if fw_old:
        if fw_old.check_acl("@view", user=up.user):
            raise FileExistsError(
                "A file with hash {} already exists".format(
                    fw_old.checksum_upload_sha1
                ),
            )
        raise FileExistsError(
            "Another user has already uploaded this firmware",
        )

    # check the guid and version does not already exist
    up.status = "Checking for duplicates…"
    fws = db.session.query(Firmware).all()
    fws_already_exist: List[Firmware] = []
    for md in ufile.fw.mds:
        if not md.guids:
            continue
        provides_value = md.guids[0].value
        fw = _filter_fw_by_id_guid_version(
            fws, md.appstream_id, provides_value, md.version
        )
        if fw:
            fws_already_exist.append(fw)

    # all the components existed, so build an error out of all the versions
    if len(fws_already_exist) == len(ufile.fw.mds):
        if up.user.check_acl("@robot") and up.auto_delete:
            for fw in fws_already_exist:
                if fw.remote.is_public:
                    raise PermissionError(
                        "Firmware {} cannot be autodeleted as is in remote {}".format(
                            fw.firmware_id, fw.remote.name
                        )
                    )
                if fw.user.user_id != up.user.user_id:
                    raise PermissionError("Firmware was not uploaded by this user")
            for fw in fws_already_exist:
                # auto-delete due to robot upload
                _firmware_delete(fw)
        else:
            versions_for_display: List[str] = []
            for fw in fws_already_exist:
                for md in fw.mds:
                    if not md.version_display in versions_for_display:
                        versions_for_display.append(md.version_display)
            raise FileExistsError(
                "A firmware file for this device with "
                "version {} already exists".format(",".join(versions_for_display)),
            )

    # allow plugins to copy any extra files from the source archive
    if not ufile.cabarchive_upload:
        raise FileNotSupported("Failed to store uploaded file")
    up.status = "Copying archive…"
    for cffile in ufile.cabarchive_upload.values():
        ploader.archive_copy(ufile.cabarchive_repacked, cffile)

    # create parent firmware object
    up.status = "Compressing archive…"
    settings = _get_settings()
    target = up.target
    fw = ufile.fw
    fw.blob = ufile.cabarchive_repacked.save()
    fw.revisions.append(up.revision)
    if revision != up.revision:
        fw.revisions.append(revision)
    fw.vendor = up.vendor
    fw.vendor_odm = up.user.vendor
    fw.user = up.user
    fw.addr = up.addr
    fw.remote = remote
    fw.checksum_signed_sha1 = hashlib.sha1(fw.blob).hexdigest()
    fw.checksum_signed_sha256 = hashlib.sha256(fw.blob).hexdigest()
    fw.is_dirty = True
    fw.failure_minimum = settings["default_failure_minimum"]
    fw.failure_percentage = settings["default_failure_percentage"]
    fw.mark_dirty()
    for md in fw.mds:
        for issue in md.issues:
            issue.user = up.user

    # fix name
    for md in fw.mds:
        name_fixed = _fix_component_name(md.name, up.vendor.display_name)
        if name_fixed != md.name:
            md.name = name_fixed

    # verify each component has a version format
    for md in fw.mds:
        if not md.verfmt:
            if md.protocol and md.protocol.verfmt:
                md.verfmt = md.protocol.verfmt
            elif (
                md.fw.vendor.verfmt
                and md.protocol
                and md.protocol.value == "org.uefi.capsule"
            ):
                md.verfmt = md.fw.vendor.verfmt

    # add to database
    fw.events.append(
        FirmwareEvent(
            kind=FirmwareEventKind.UPLOADED.value, remote=remote, user=up.user
        )
    )
    try:
        db.session.add(fw)
        db.session.commit()
    except IntegrityError as e:
        db.session.rollback()
        raise FileExistsError from e

    # ensure the test has been added for the firmware type
    up.status = "Ensuring tests…"
    ploader.ensure_test_for_fw(fw)

    # sync everything we added
    db.session.commit()

    # asynchronously run
    up.status = "Running tests…"
    _async_test_run_for_firmware.apply_async(args=(fw.firmware_id,))

    # send out emails to anyone interested
    if fw.get_possible_users_to_email:
        up.status = "Emailing users…"
    for u in fw.get_possible_users_to_email:
        if u == up.user:
            continue
        if u.get_action("notify-upload-vendor") and u.vendor == fw.vendor:
            send_email(
                "[LVFS] Firmware has been uploaded",
                u.email_address,
                render_template(
                    "email-firmware-uploaded.txt", user=u, user_upload=up.user, fw=fw
                ),
            )
        elif u.get_action("notify-upload-affiliate"):
            send_email(
                "[LVFS] Firmware has been uploaded by affiliate",
                u.email_address,
                render_template(
                    "email-firmware-uploaded.txt", user=u, user_upload=up.user, fw=fw
                ),
            )

    # invalidate
    if target == "embargo":
        remote.is_dirty = True
        up.user.vendor.remote.is_dirty = True
        db.session.commit()

    # synchronously sign
    if sign:
        up.status = "Signing file…"
        _sign_fw(fw)
        up.status = "Signing metadata…"
        fw.remote.is_dirty = True
        _regenerate_and_sign_metadata_remote(fw.remote)

    # success
    return fw


def _upload_firmware(up: Upload) -> None:

    # set the timestamps automatically
    with up:
        up.status = "Parsing file…"
        try:
            with db.session.no_autoflush:  # pylint: disable=no-member
                _ = _upload_firmware_raw(up)
        except (FileNotSupported, PermissionError, FileExistsError, KeyError) as e:
            up.status = str(e)
            return

        # success
        up.status = "Completed"


@tq.task(max_retries=3, default_retry_delay=5, task_time_limit=60)
def _async_upload_firmware(upload_id):
    up = db.session.query(Upload).filter(Upload.upload_id == upload_id).first()
    if not up:
        return
    _upload_firmware(up)
