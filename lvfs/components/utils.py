#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pyxxxxxlint: disable=singleton-comparison,protected-access

from lvfs.components.models import Component
from uswid import uSwidIdentity, uSwidEntityRole, uSwidLink, uSwidEntity


def _build_swid_identity_root(md: Component) -> uSwidIdentity:
    """ create identity for the index with original vendor and LVFS """

    identity = uSwidIdentity(
        tag_id=md.appstream_id,
        software_name=md.name,
        software_version=md.version_display,
    )
    identity.add_entity(
        uSwidEntity(
            name="LVFS",
            regid="fwupd.org",
            roles=[uSwidEntityRole.TAG_CREATOR, uSwidEntityRole.DISTRIBUTOR],
        )
    )
    identity.add_entity(
        uSwidEntity(
            name=md.fw.vendor.display_name,
            regid=md.fw.vendor.regid,
            roles=[uSwidEntityRole.SOFTWARE_CREATOR],
        )
    )
    for swid in md.swids:
        identity.add_link(
            uSwidLink(rel="component", href="swid:{}".format(swid.tag_id))
        )
    return identity
