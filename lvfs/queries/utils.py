#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=singleton-comparison

import tempfile

import yara
from uefi_r2 import UefiAnalyzer, UefiRule, UefiScanner

from lvfs import db, tq

from lvfs.components.models import Component, ComponentShard
from lvfs.firmware.models import Firmware
from lvfs.metadata.models import Remote

from .models import YaraQuery, YaraQueryResult


@tq.task(max_retries=3, default_retry_delay=60, task_time_limit=6000)
def _async_query_run(yara_query_id):
    query = (
        db.session.query(YaraQuery)
        .filter(YaraQuery.yara_query_id == yara_query_id)
        .first()
    )
    if not query:
        return
    _query_run(query)


def _query_run_shard_uefi_r2(
    query: YaraQuery, md: Component, shard: ComponentShard
) -> None:

    if not shard.blob:
        return
    if shard.blob[0:2] != b"MZ":
        return
    with UefiAnalyzer(blob=shard.blob) as uefi_analyzer:
        scanner = UefiScanner(uefi_analyzer, query.rules)
        if scanner.result:
            query.results.append(YaraQueryResult(md=md, shard=shard, result=shard.guid))


def _query_run_shard_yara(
    query: YaraQuery, md: Component, shard: ComponentShard
) -> None:

    if not shard.blob:
        return
    matches = query.rules.match(data=shard.blob)
    for match in matches:
        msg = match.rule
        for string in match.strings:
            if len(string) == 3:
                try:
                    msg += ": found {}".format(string[2].decode())
                except UnicodeDecodeError:
                    pass
        query.results.append(YaraQueryResult(md=md, shard=shard, result=msg))


def _query_run_component_yara(query: YaraQuery, md: Component) -> None:
    if not md.blob:
        return
    matches = query.rules.match(data=md.blob)
    for match in matches:
        msg = match.rule
        for string in match.strings:
            if len(string) == 3:
                try:
                    msg += ": found {}".format(string[2].decode())
                except UnicodeDecodeError:
                    pass
        query.results.append(YaraQueryResult(md=md, result=msg))

    # one more thing scanned
    query.total += 1

    # unallocate the cached blob as it's no longer needed
    md.blob = None


def _query_setup_yara(query: YaraQuery) -> None:
    try:
        query.rules = yara.compile(source=query.value)
    except yara.SyntaxError as e:
        raise NotImplementedError("Failed to compile rules: {}".format(str(e))) from e
    for line in query.value.replace("{", "\n").split("\n"):
        if line.startswith("rule "):
            query.title = line[5:]
            break
    db.session.commit()


def _query_setup_uefi_r2(query: YaraQuery) -> None:
    with tempfile.NamedTemporaryFile(
        mode="wb", prefix="uefi_r2_rule", suffix=".yml", dir=None, delete=True
    ) as f:
        f.write(query.value.encode())
        f.flush()
        query.rules = UefiRule(f.name)
        if not query.rules.name:
            raise NotImplementedError("No name found for rule")
        query.title = query.rules.name
        if query.rules.volume_guids:
            for guid in query.rules.volume_guids:
                query.volume_guids.append(guid.lower())
    db.session.commit()


def _query_run(query: YaraQuery) -> None:
    print("processing query {}...".format(query.yara_query_id))

    # use different vfuncs for YARA and UEFI R2
    if not query.kind or query.kind == "yara":
        query.setup = _query_setup_yara
        query.run_shard = _query_run_shard_yara
        query.run_component = _query_run_component_yara
    elif query.kind == "uefi_r2":
        query.setup = _query_setup_uefi_r2
        query.run_shard = _query_run_shard_uefi_r2
    if query.setup:
        try:
            query.setup(query)
        except NotImplementedError as e:
            query.error = str(e)
            db.session.commit()
            return

    # restrict these to specific GUIDs
    for line in query.value.split("\n"):
        idx = line.find("X-Volume-GUIDs")
        if idx != -1:
            for guid in line[idx + len("X-Volume-GUIDs") + 1 :].split(","):
                query.volume_guids.append(guid.strip().lower())

    # run query
    with query:

        # don't allow scanning *all* files as it takes ~14h of CPU time
        if not query.user.check_acl("@admin") and not query.volume_guids:
            query.error = "no X-Volume-GUIDs specified"
            return

        component_ids = [
            x[0]
            for x in db.session.query(Component.component_id)
            .join(Firmware)
            .join(Remote)
            .filter(Remote.name == "stable")
            .all()
        ]
        if component_ids:
            fraction = 100 / len(component_ids)
            for idx, component_id in enumerate(component_ids):
                md = (
                    db.session.query(Component)
                    .filter(Component.component_id == component_id)
                    .one()
                )
                print("running query on {} ({})".format(component_id, md.appstream_id))

                # scan each shard
                for shard in md.shards:
                    if query.run_shard and shard.guid in query.volume_guids:
                        query.run_shard(query, md, shard)
                        query.total += 1
                    shard.blob = None

                # scan each component
                if query.run_component:
                    query.run_component(query, md)
                query.found = len(query.results)
                query.percentage = (idx + 1) * fraction
                db.session.commit()


def _query_run_all() -> None:

    # get all pending queries
    pending = (
        db.session.query(YaraQuery)
        .filter(YaraQuery.started_ts == None)
        .filter(YaraQuery.error == None)
    )
    for query in pending:
        _query_run(query)
