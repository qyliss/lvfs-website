"""

Revision ID: 4c9df1b7b40d
Revises: b04dba4902e1
Create Date: 2021-03-23 12:17:35.934061

"""

# revision identifiers, used by Alembic.
revision = '4c9df1b7b40d'
down_revision = 'b04dba4902e1'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('firmware_events', sa.Column('remote_old_id', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'firmware_events', 'remotes', ['remote_old_id'], ['remote_id'])


def downgrade():
    op.drop_constraint(None, 'firmware_events', type_='foreignkey')
    op.drop_column('firmware_events', 'remote_old_id')
