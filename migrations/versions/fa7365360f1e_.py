"""

Revision ID: fa7365360f1e
Revises: a51621108a8f
Create Date: 2021-09-22 12:39:21.162012

"""

# revision identifiers, used by Alembic.
revision = "fa7365360f1e"
down_revision = "a51621108a8f"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column("users", sa.Column("subgroup", sa.Text(), nullable=True))
    op.add_column("vendors", sa.Column("subgroups", sa.Text(), nullable=True))


def downgrade():
    op.drop_column("vendors", "subgroups")
    op.drop_column("users", "subgroup")
