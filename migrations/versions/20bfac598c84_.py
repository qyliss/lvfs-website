"""

Revision ID: 20bfac598c84
Revises: ec2fe30db198
Create Date: 2021-11-23 11:51:24.648847

"""

# revision identifiers, used by Alembic.
revision = "20bfac598c84"
down_revision = "f80b9c7b7267"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        "component_tags",
        sa.Column("tag_id", sa.Integer(), nullable=False),
        sa.Column("component_id", sa.Integer(), nullable=False),
        sa.Column("value", sa.Text(), nullable=False),
        sa.ForeignKeyConstraint(
            ["component_id"],
            ["components.component_id"],
        ),
        sa.PrimaryKeyConstraint("tag_id"),
    )
    op.create_index(
        op.f("ix_component_tags_component_id"),
        "component_tags",
        ["component_id"],
        unique=False,
    )


def downgrade():
    op.drop_index(op.f("ix_component_tags_component_id"), table_name="component_tags")
    op.drop_table("component_tags")
