"""

Revision ID: b04dba4902e1
Revises: 2328bb1468ac
Create Date: 2021-03-19 10:53:28.872775

"""

# revision identifiers, used by Alembic.
revision = 'b04dba4902e1'
down_revision = '2328bb1468ac'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('component_issues', sa.Column('timestamp', sa.DateTime(), nullable=True))
    op.add_column('component_issues', sa.Column('user_id', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'component_issues', 'users', ['user_id'], ['user_id'])


def downgrade():
    op.drop_constraint(None, 'component_issues', type_='foreignkey')
    op.drop_column('component_issues', 'user_id')
    op.drop_column('component_issues', 'timestamp')
