"""

Revision ID: 47cb29dd15e0
Revises: 20bfac598c84
Create Date: 2021-11-24 13:17:39.764305

"""

# revision identifiers, used by Alembic.
revision = "47cb29dd15e0"
down_revision = "20bfac598c84"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column(
        "vendors", sa.Column("allow_component_tags", sa.Boolean(), nullable=True)
    )


def downgrade():
    op.drop_column("vendors", "allow_component_tags")
