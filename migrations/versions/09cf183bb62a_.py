"""

Revision ID: 09cf183bb62a
Revises: cac290ed7074
Create Date: 2021-10-22 12:58:15.576787

"""

# revision identifiers, used by Alembic.
revision = "09cf183bb62a"
down_revision = "cac290ed7074"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.drop_column("categories", "fallbacks")
    op.drop_column("components", "project_license")
    op.drop_column("components", "developer_name")
    op.drop_column("components", "metadata_license")
    op.drop_column("vendors", "description")


def downgrade():
    op.add_column(
        "vendors",
        sa.Column("description", sa.TEXT(), autoincrement=False, nullable=True),
    )
    op.add_column(
        "components",
        sa.Column("metadata_license", sa.TEXT(), autoincrement=False, nullable=True),
    )
    op.add_column(
        "components",
        sa.Column("developer_name", sa.TEXT(), autoincrement=False, nullable=True),
    )
    op.add_column(
        "components",
        sa.Column("project_license", sa.TEXT(), autoincrement=False, nullable=True),
    )
    op.add_column(
        "categories",
        sa.Column("fallbacks", sa.TEXT(), autoincrement=False, nullable=True),
    )
