#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=no-self-use

import struct
import uuid

from lvfs.pluginloader import PluginBase, PluginSettingBool
from lvfs.tests.models import Test


class Plugin(PluginBase):
    def __init__(self):
        PluginBase.__init__(self, "uefi-capsule")
        self.name = "UEFI Capsule"
        self.summary = "Check the UEFI capsule header and file structure"
        self.settings.append(
            PluginSettingBool(
                key="uefi_capsule_check_header", name="Enabled", default=True
            )
        )

    def require_test_for_md(self, md):

        # only run for capsule updates
        if not md.protocol:
            return False
        if not md.blob:
            return False
        return md.protocol.value == "org.uefi.capsule"

    def ensure_test_for_fw(self, fw):

        # add if not already exists
        test = fw.find_test_by_plugin_id(self.id)
        if not test:
            test = Test(plugin_id=self.id, waivable=True)
            fw.tests.append(test)

    def run_test_on_md(self, test, md):

        # unpack the header
        try:
            data = struct.unpack("<16sIII", md.blob[:28])
        except struct.error:
            test.add_fail("FileSize", "0x%x" % len(md.blob))
            # we have to abort here, no further tests are possible
            return

        # check the GUID
        guid = str(uuid.UUID(bytes_le=data[0]))
        if guid == "00000000-0000-0000-0000-000000000000":
            test.add_fail("GUID", "{} is not valid".format(guid))
        elif guid in [
            "3b6686bd-0d76-4030-b70e-b5519e2fc5a0",  # EFI_CAPSULE_GUID
            "4a3ca68b-7723-48fb-803d-578cc1fec44d",  # EFI_SIGNED_CAPSULE_GUID
        ]:
            test.add_fail(
                "GUID",
                "{} is the hardcoded EFI_CAPSULE_GUID when it is supposed to "
                "be the GUID found in the ESRT table".format(guid),
            )
        else:
            referenced_guids = [gu.value for gu in md.guids]
            if guid in referenced_guids:
                test.add_pass("GUID", guid)
            else:
                test.add_fail(
                    "GUID",
                    "{} not found in {}".format(guid, ",".join(referenced_guids)),
                )

        # check the header size
        if data[1] == 0:
            test.add_fail("HeaderSize", "0x%x" % data[1])
        else:
            test.add_pass("HeaderSize", "0x%x" % data[1])

        # check if the flags are sane
        CAPSULE_FLAGS_PERSIST_ACROSS_RESET = 0x00010000
        CAPSULE_FLAGS_POPULATE_SYSTEM_TABLE = 0x00020000
        CAPSULE_FLAGS_INITIATE_RESET = 0x00040000
        if (
            data[2] & CAPSULE_FLAGS_POPULATE_SYSTEM_TABLE > 0
            and data[2] & CAPSULE_FLAGS_PERSIST_ACROSS_RESET == 0
        ):
            test.add_fail(
                "Flags",
                "0x%x -- POPULATE_SYSTEM_TABLE requires PERSIST_ACROSS_RESET" % data[2],
            )
        elif (
            data[2] & CAPSULE_FLAGS_INITIATE_RESET > 0
            and data[2] & CAPSULE_FLAGS_PERSIST_ACROSS_RESET == 0
        ):
            test.add_fail(
                "Flags",
                "0x%x -- INITIATE_RESET requires PERSIST_ACROSS_RESET" % data[2],
            )
        else:
            test.add_pass("Flags", "0x%x" % data[2])

        # check the capsule image size
        if data[3] == len(md.blob):
            test.add_pass("CapsuleImageSize", "0x%x" % data[3])
        else:
            test.add_fail(
                "CapsuleImageSize",
                "0x%x does not match file size 0x%x" % (data[3], len(md.blob)),
            )
