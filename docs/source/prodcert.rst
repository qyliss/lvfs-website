Product Certification
#####################

.. figure:: img/prodcert-title.png
    :align: center
    :width: 100%
    :alt: logo

Introduction
============

We want to make it easy for ODMs and OEMs to choose components that already have fwupd plugin support.
This will do a few things:

 * The onus is pushed onto the IHV to maintain the plugin not the OEM, ODM or Linux distributor (e.g. Red Hat)
 * The ODM and OEM will prefer components that do not require any software development work to pass
   the Works With ChromeBook (WWCB) and Red Hat Enterprise Linux (RHEL) hardware certifications
 * Having a fwupd plugin will be seen as a commercial advantage for the IHV

There are two versions of the *fwupd friendly firmware* certification, one for devices that will
only accept signed firmware (``signed-payload``) and another for insecure hardware that does not
implement cryptographic signing (``unsigned-payload``).
Either is fine from a fwupd plugin point-of-view, but some OEMs will have a policy that forces them
to choose hardware that cannot be altered by the end user.

.. note::

  Consumer devices end-users buy from the store are not suitable for *fwupd friendly firmware*,
  and already have `device pages <https://fwupd.org/lvfs/devices/>`_ on the LVFS.

Requirements
============

To register a device for *fwupd friendly firmware* the original silicon vendor must have an existing
LVFS vendor account, and also provide:

 * The device model, e.g. ``CX2098X``
 * The link to the upstream fwupd plugin that handles this device type, e.g. ``https://github.com/fwupd/fwupd/tree/main/plugins/synaptics-cxaudio``
 * The hash and signature algorithm used for firmware signing, e.g. ``SHA256+RSA2048``, or ``None``

To add a device to the `certification page <https://fwupd.org/lvfs/vendors/prodcerts>`_,
please send an email to the
`mailing list <https://lists.linuxfoundation.org/mailman/listinfo/lvfs-general>`_
with the required details.
On meeting the requirements, the entry will be added and then the vendor is allowed use the signed or
unsigned *fwupd friendly firmware* logo as required.

.. figure:: img/prodcert-signed.png
    :align: center
    :alt: *fwupd friendly firmware*

    logo for *fwupd friendly firmware* (signed)

.. figure:: img/prodcert-unsigned.png
    :align: center
    :alt: *fwupd friendly firmware*

    logo for *fwupd friendly firmware* (unsigned)

.. note::

  Vendors should not have a “generic” *fwupd friendly firmware* assigned to them, as a vendor may
  have multiple devices with different update protocols. e.g. Synaptics has ``cxaudio``, ``mst``,
  ``prometheus`` and ``cape`` protocols, each with a different fwupd plugin.

Conclusion
==========

Vendors using the *fwupd friendly firmware* logo mark will make it easier for product creators to
support firmware updates for Linux users.
The burden of development moves earlier to the IHVs rather than later to the OEMs.
OEMs can verify the *fwupd friendly firmware* certification and compare hardware using the public
pages on the LVFS.
